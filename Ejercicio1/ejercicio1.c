#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main() {
  int fd, salida;
  char buffer[50];
  ssize_t tamanioMensaje;
  char mensaje[] = "Sistemas Operativos 2020\n";
  tamanioMensaje = strlen(mensaje);
  char ruta[] = "mensaje_a_imprimir_en_pantalla";
  //se crea el archivo si no existe y se escribe en él, el mensaje a mostrar en pantalla
  fd = open(ruta, O_RDWR|O_CREAT,S_IRUSR|S_IWUSR);
  salida = write(fd,mensaje,tamanioMensaje);
  close(fd);

//se procede a abrir y leer el archivo creado
  fd = open(ruta,O_RDONLY);
  salida = read(fd,buffer,salida);
  salida = write(1,buffer,salida);

  close(fd);
  return 0;
}
